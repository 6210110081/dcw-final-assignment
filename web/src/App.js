import './App.css';
import axios from 'axios'
import FacebookLogin from 'react-facebook-login';
import TableCovid from './components/tableCovid'
import React, { useState } from 'react';
import { StyledEngineProvider } from '@mui/material/styles';

<script>{document.body.style.backgroundColor = "lightgray"}</script>

axios.interceptors.request.use(function (config) {
  const token = sessionStorage.getItem('access_token')
  if (token)
    config.headers['Authorization'] = `Bearer ${token}`
  return config
}, function (err) {
  return Promise.reject(err)
})

const responseFacebook = async (response) => {
  if (response.accessToken) {
    console.log('log in with accessToken= ' + response.accessToken)
    let result = await axios.post('http://localhost:8000/api/login', {
      token: response.accessToken
    })
    console.log(result.data)
    sessionStorage.setItem('access_token', result.data.access_token)
  }
}



function App() {
  let [result, setResult] = useState(null)
  let [resultCallinfo, setResultCallinfo] = useState(null)

  const callInfoAPI = async () => {
    let resultCallinfo = await axios.get('http://localhost:8000/api/info')
    setResultCallinfo(resultCallinfo.data)
    console.log(resultCallinfo.data)
  }

  const callCovidByProvinces = async () => {
    let result = await axios.get('http://localhost:8000/api/covid/byProvinces')
    setResult(result.data)
    console.log(result.data)
  }

  const callCovidAllCaseDay = async () => {
    let result = await axios.get('http://localhost:8000/api/covid/allCase')
    setResult(result.data)
    console.log(result.data)
  }

  return (
    <div>
      <div style={{ display: 'flex', justifyContent: 'space-around' }}>
        <button onClick={callCovidByProvinces}>Call Covid By Provinces</button>
        <button onClick={callCovidAllCaseDay}>Call Covid All Case To Day</button>
        <button onClick={callInfoAPI}>Call Info</button>
        <FacebookLogin
          appId="569640164007192"
          autoLoad={true}
          fields="name,email,picture"
          callback={responseFacebook}
        />
      </div>
      {result &&
        <StyledEngineProvider injectFirst>
          <TableCovid value={result} />
        </StyledEngineProvider>}
    </div>
  );
}

export default App;
