import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';


export default function BasicTable(props) {
    console.log(props)
    return (
        <TableContainer id="myDiv" component={Paper}>
            <Table sx={{ minWidth: 500 }} aria-label="simple table">
                <TableHead>
                        <TableRow id="miTabla">
                            <TableCell>{props.value.length == 1 ? "วันที่" : "จังหวัด"}</TableCell>
                            <TableCell align="right">รายใหม่&nbsp;(คน)</TableCell>
                            <TableCell align="right">ยอดติดโควิดรวม&nbsp;(คน)</TableCell>
                            <TableCell align="right">เสียชีวิต&nbsp;(คน)</TableCell>
                            <TableCell align="right">เสียชีวิตรวม&nbsp;(คน)</TableCell>
                        </TableRow>
                </TableHead>
                <TableBody>
                    {props.value.map((prop) => (
                        <TableRow
                            key={prop.province}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                            <TableCell component="th" scope="row">{props.value.length == 1 ? prop.txn_date:prop.province}</TableCell>
                            <TableCell align="right">{prop.new_case}</TableCell>
                            <TableCell align="right">{prop.total_case}</TableCell>
                            <TableCell align="right">{prop.new_death}</TableCell>
                            <TableCell align="right">{prop.total_death}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
