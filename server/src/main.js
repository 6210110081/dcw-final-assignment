const express = require('express')
const bodyParser = require('body-parser')
const axios = require('axios')
const app = express()
const cors = require('cors')
const jwt = require('jsonwebtoken')
const port = 8000

const TOKEN_SECRET = '85bef232df7d85f5d43be8e2e28483cfd4fa1297f3272b4ab6a66fdd554ee888c95c784148fa538b3655ea6d1f94c96a290744835f39a66f05915efb4b82a8a8'

const authenticated = async (req, res, next) => {
  const auth_header = req.headers['authorization']
  const token = auth_header && auth_header.split(' ')[1]
  if (!token)
    return res.sendStatus(401)
  jwt.verify(token, TOKEN_SECRET, (err, info) => {
    if (err) return res.sendStatus(403)
    req.name = info.name
    req.email = info.email
    req.id = info.id
    next()
  })
}

app.use(cors())

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/api/info', authenticated, (req, res) => {
  res.send({ username: req.name, email: req.email, user_id: req.id })
})

app.post('/api/login', bodyParser.json(), async (req, res) => {
  let token = req.body.token
  let result = await axios.get('https://graph.facebook.com/me', {
    params: {
      fields: 'id,name,email',
      access_token: token
    }
  })
  if (!result.data.id) {
    res.sendStatus(403)
    return
  }
  let data = {
    email: result.data.email,
    name: result.data.name,
    id: result.data.id
  }
  let access_token = jwt.sign(data, TOKEN_SECRET, { expiresIn: '1800s' })
  res.send({ access_token, username: data.name })
})

app.get('/api/covid/byProvinces', async (req, res) => {
  let result = await axios.get('https://covid19.ddc.moph.go.th/api/Cases/today-cases-by-provinces')
  console.log(result)
  res.send(result.data)
})

app.get('/api/covid/allCase', async (req, res) => {
  let result = await axios.get('https://covid19.ddc.moph.go.th/api/Cases/today-cases-all')
  console.log(result)
  res.send(result.data)
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

